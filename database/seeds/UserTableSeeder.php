<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Ahmet Talut",
            'surname' => "Taşgıran",
            'username' => "ataluttasgiran",
            'avatar' => "32132139efe873.png",
            'birthdate' => "15.10.1996",
            'biography' => "Lorem ipsum dolor sit amet",
            'email' => "ahmet@localhost.com",
            'password' => bcrypt('12345'),
            'level' => "1",
        ]);
    }
}

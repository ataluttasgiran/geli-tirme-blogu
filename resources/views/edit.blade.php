@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (Session::has('response'))
                            <div class="alert alert-info">{{ Session::get('response') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Başlık</th>
                                <th>İçerik Özeti</th>
                                <th>Yayın Tarihi</th>
                                <th>Seçenekler</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <th scope="row">{{$post->id}}</th>
                                <td>{{$post->title}}</td>
                                <td>{{ str_limit(strip_tags($post->content), $limit = 200, $end = '...') }}</td>
                                <td><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans() ?></td>
                                <td><a href="{{URL::to('/sil')}}/{{$post->id}}"><button class="btn btn-danger">Sil</button></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

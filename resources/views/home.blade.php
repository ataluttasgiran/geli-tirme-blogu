@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (Session::has('response'))
                        <div class="alert alert-info">{{ Session::get('response') }}</div>
                    @endif
                    <form action="{{URL::to('/')}}/yonetici" method="post" enctype="multipart/form-data">
                        {{ csrf_field()}}
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Geliştirme Başlığı <span class="label label-danger">{{ $errors->first('title') }}</span></span>
                                <input type="text" class="form-control" name="title" placeholder="Geliştirme Başlığı" aria-describedby="basic-addon1">
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="content" class="control-label">Geliştirme içeriği <span class="label label-danger">{{ $errors->first('content') }}</span></label>
                            <textarea name="content" class="form-control" id="content" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Öne Çıkan Görsel</label>
                            <input type="file" name="thumbnail" id="exampleInputFile">
                            <p class="help-block">İsteğe bağlı</p>
                        </div>
                        <div class="form-group">
                            <label for="content" class="control-label">Embed Code (İsteğe bağlıdır. Embed kod eklenirse öne çıkan görsel varsa bile eklediğiniz içerik gözükür.)</label>
                            <textarea name="embed_code" class="form-control" id="content" rows="3"></textarea>
                        </div>
                        <button class="btn btn-primary" type="submit">Yeni Geliştirme Ekle</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

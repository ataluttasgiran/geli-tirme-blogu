@extends("layouts.index")
@section("content")


    <section id="cd-timeline" class="cd-container">

        @foreach($posts as $post)
            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    @if($post->embed_code!="")
                        <img src="{{URL::to('/')}}/assets/img/cd-icon-movie.svg" alt="Picture">
                    @else
                        @if($post->thumbnail!="")
                            <img src="{{URL::to('/')}}/assets/img/cd-icon-picture.svg" alt="Picture">
                        @else
                            <img src="{{URL::to('/')}}/assets/img/cd-icon-location.svg" alt="Picture">
                        @endif
                    @endif
                </div>
                <!-- cd-timeline-img -->

                <div class="cd-timeline-content">
                    <h2>{{$post->title}}</h2>
                    <p>
                        {!! $post->content !!}
                    </p>
                    @if($post->embed_code!="")
                        {!! $post->embed_code !!}
                    @else
                        @if($post->thumbnail!="")
                            <a href="{{URL::to('/')}}/uploads/{{$post->thumbnail}}" target="_blank"><img
                                        style="width:100%; max-height:300px;"
                                        src="{{URL::to('/')}}/uploads/{{$post->thumbnail}}" alt=""></a>
                        @else
                        @endif
                    @endif
                    <span class="cd-date"> <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($post->created_at))->diffForHumans() ?></span>
                </div>
                <!-- cd-timeline-content -->
            </div> <!-- cd-timeline-block -->
        @endforeach


    </section> <!-- cd-timeline -->
@stop
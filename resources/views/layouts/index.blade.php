<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Droid+Serif|Open+Sans:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/style.css"> <!-- Resource style -->
    <script src="{{URL::to('/')}}/assets/js/modernizr.js"></script> <!-- Modernizr -->

    <title>ADOBLOG ~ Geliştirme Blogu</title>
</head>
<body>
<header>
    <h1><strong>ADOBLOG Sistem</strong> Geliştirme Blogu</h1>
</header>
@yield("content")
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{URL::to('/')}}/assets/js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>
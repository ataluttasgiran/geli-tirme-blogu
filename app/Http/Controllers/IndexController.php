<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use View;
use Intervention\Image\Facades\Image;
use App\Post;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        Carbon::setLocale('tr');
        $posts = Post::orderby("id","desc")->get();
        return view('index', compact('posts'));

    }
}

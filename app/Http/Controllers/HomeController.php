<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use View;
use Intervention\Image\Facades\Image;
use App\Post;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */

    public function home()
    {
        return view('home');
    }
    public function edit()
    {
        Carbon::setLocale('tr');
        $posts = Post::orderby("id","desc")->get();
        return view('edit', compact('posts'));
    }
    public function delete($id)
    {
        $posts = Post::destroy($id);
        return Redirect::to('/duzenle')->with('response', 'Silme işlemi başarılı');
    }
    public function newUpdate(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $Post = new Post();
        $Post->title = $request->input('title');
        $Post->content = $request->input("content");
        $Post->embed_code = $request->input("embed_code");
        $image_ex = Input::file('thumbnail');
        if($image_ex!=""){
            $image = Input::file('thumbnail');
            $fileArray = array('thumbnail' => $image);
        $rules = array(
            'thumbnail' => 'mimes:jpeg,jpg,png,gif|max:2048' // max 10000kb
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails())
        {

            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        } else
        {
            $filename  = time() .'-'. $image->getClientOriginalName();
            $path = public_path('uploads/' . $filename);
            Image::make($image->getRealPath())->save($path);
            $Post->thumbnail = $filename;
            $sonuc = $Post -> save();
            if(!$sonuc){
                App::abort(500, 'Error');
            }else{
                return Redirect::to('/duzenle')->with('response', 'Kayıt Başarılı');
            }
        }
        }else{
            $sonuc = $Post -> save();
            if(!$sonuc){
                App::abort(500, 'Error');
            }else{
                return Redirect::to('/duzenle')->with('response', 'Kayıt Başarılı');
            }
        }
}
}
